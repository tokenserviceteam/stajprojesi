package com.property;

import java.io.IOException;
import java.util.Properties;



public class PropertiesFile {
	
	public  void connectionProperties(){		
		Properties propertyObject = new Properties();	
		try {			
			propertyObject.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("propertyFile.properties"));
				PropertyConstant.deriverClas = propertyObject.getProperty("driverClas");
				PropertyConstant.dbms = propertyObject.getProperty("dbms");
				PropertyConstant.server = propertyObject.getProperty("server");
				PropertyConstant.portNumber = propertyObject.getProperty("portNumber");
				PropertyConstant.database = propertyObject.getProperty("database");
				PropertyConstant.username = propertyObject.getProperty("username");
				PropertyConstant.password = propertyObject.getProperty("password");	
				PropertyConstant.dbDataSourceEnabled=Boolean.parseBoolean(propertyObject.getProperty("dbDataSourceEnabled"));
				
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}