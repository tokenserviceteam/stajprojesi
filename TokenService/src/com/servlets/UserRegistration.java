package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.registry.UserBean;
import com.registry.UserOperationImpl;


@WebServlet("/userregistration")
public class UserRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	static Logger log= Logger.getLogger(UserRegistration.class.getName());
	UserBean newUser=new UserBean();
	UserOperationImpl UserOperationImplObj=new UserOperationImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		newUser.setName(request.getParameter("name"));
		newUser.setSurname(request.getParameter("surname"));			
		newUser.setUserName(request.getParameter("username"));
		newUser.setPassword(request.getParameter("password"));
		newUser.setRoleName("user");
		UserOperationImplObj.addUser(newUser);		
		
		
		log.trace("Trace Message!");
	    log.debug("Debug Message!");
	    log.info("Info Message!");
	    log.warn("Warn Message!");
	    log.error("Error Message!");
	    log.fatal("Fatal Message!");
		
		
		response.sendRedirect("giris/userPage.html");
	}

}
