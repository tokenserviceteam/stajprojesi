package com.registry;

import java.util.List;
public interface UserOperationInterface {
	public List<UserBean> userList();
	public List<UserBean> tokenList();
	public List<UserBean> userInformation();
	public void addUser(UserBean newUser);
	public void updateUser(UserBean newUser);
	public void deleteUser(String userName);

}
