package com.registry;


public class UserBean {
	private String userName;
	private String name;
	private String surname;
	private String password;
	private String roleName;
	private String tokenText;
	private String tokenSetDate;
	private String tokenExpiryDate;
	
		
	public UserBean(){		
	}
	
	public UserBean(String tckn,String name,String surname,String password,String role){
		this.userName=tckn;
		this.name=name;
		this.surname=surname;	
		this.password=password;
		this.roleName=role;
	}
	
	public void BeanKisiBilgiAl(String tckn,String name,String surname,String password,String role){
		this.userName=tckn;
		this.name=name;
		this.surname=surname;	
		this.password=password;	
		this.roleName=role;
	}

	public UserBean(String tokenText,String tckn,String tokenSetDate,String tokenExpiryDate){
		this.tokenText=tokenText;
		this.userName=tckn;
		this.tokenSetDate=tokenSetDate;	
		this.tokenExpiryDate=tokenExpiryDate;
	}
	
	public void BeanTokenBilgiAl(String tokenText,String tckn,String tokenSetDate,String tokenExpiryDate){
		this.tokenText=tokenText;
		this.userName=tckn;
		this.tokenSetDate=tokenSetDate;	
		this.tokenExpiryDate=tokenExpiryDate;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String tckn) {
		this.userName = tckn;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getTokenText() {
		return tokenText;
	}

	public void setTokenText(String tokenText) {
		this.tokenText = tokenText;
	}

	public String getTokenSetDate() {
		return tokenSetDate;
	}

	public void setTokenSetDate(String tokenSetDate) {
		this.tokenSetDate = tokenSetDate;
	}

	public String getTokenExpiryDate() {
		return tokenExpiryDate;
	}

	public void setTokenExpiryDate(String tokenExpiryDate) {
		this.tokenExpiryDate = tokenExpiryDate;
	}
	
	

}
