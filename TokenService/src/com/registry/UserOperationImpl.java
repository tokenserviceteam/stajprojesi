package com.registry;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.connection.helper.ConnectionHelper;

@SuppressWarnings("serial")
@WebServlet("/UserOperationImpl")

public class UserOperationImpl extends HttpServlet implements UserOperationInterface{
	static Logger log = Logger.getLogger(UserOperationImpl.class);
	ConnectionHelper yardimci_nesne=new ConnectionHelper();
	static String activeUser;//giriş yapan kullanıcının "kullanıcı adını" bu değişkende tutuyoruz
	static String activeUserRole;//giriş yapan kullanıcının "rolünü" bu değişkende tutuyoruz
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		activeUser=request.getRemoteUser();//giriş yapan kullanıcının "kullanıcı adını" bu değişkene atıyoruz
		
		if(request.isUserInRole("admin"))
		{
			System.out.println("admin");
		}
		if(request.isUserInRole("user"))
		{
			System.out.println("user");
		}
		
		response.sendRedirect("passwordChange.jsp");
	}
	
	@Override
	public List<UserBean> userList() {
		List<UserBean> Liste = new ArrayList<UserBean>();
		try {
			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("select * from registry_table");			
			yardimci_nesne.rs=yardimci_nesne.stmt.executeQuery();				
			yardimci_nesne.baglanti().commit();	
		    log.debug("Debug Message!");
		    yardimci_nesne.rs.next();
			Liste.add(new UserBean(yardimci_nesne.rs.getString("user_name"),yardimci_nesne.rs.getString("name"),yardimci_nesne.rs.getString("surname"),yardimci_nesne.rs.getString("password"),yardimci_nesne.rs.getString("role_name")));

			yardimci_nesne.closeResources();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				yardimci_nesne.closeResources();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}	
		
		return Liste;
	}
	
	@Override
	public List<UserBean> userInformation() {
		List<UserBean> Liste = new ArrayList<UserBean>();
		try {
//			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("SELECT * FROM registry_table join users on registry_table.user_name=users.user_name join user_roles on user_roles.user_name=users.user_name WHERE users.user_name=?");			
			yardimci_nesne.stmt.setString(1, activeUser);
			yardimci_nesne.rs=yardimci_nesne.stmt.executeQuery();				
//			yardimci_nesne.baglanti().commit();	
		    log.debug("Debug Message!");
			while(yardimci_nesne.rs.next())
			{
				Liste.add(new UserBean(yardimci_nesne.rs.getString("user_name"),yardimci_nesne.rs.getString("name"),yardimci_nesne.rs.getString("surname"),yardimci_nesne.rs.getString("user_pass"),yardimci_nesne.rs.getString("role_name")));
				
			}
			yardimci_nesne.closeResources();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				yardimci_nesne.closeResources();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
		
		return Liste;
	}
	
	@Override
	public List<UserBean> tokenList() {
		List<UserBean> Liste = new ArrayList<UserBean>();
		try {
			
//			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("SELECT * FROM token_table");			
			yardimci_nesne.rs=yardimci_nesne.stmt.executeQuery();		
//			yardimci_nesne.baglanti().commit();	

		    log.debug("Debug Message!");
		    
			while(yardimci_nesne.rs.next())
			{
				Liste.add(new UserBean(yardimci_nesne.rs.getString(1),yardimci_nesne.rs.getString(2),yardimci_nesne.rs.getDate(3).toString(),yardimci_nesne.rs.getDate(4).toString()));
				
			}
			yardimci_nesne.closeResources();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				yardimci_nesne.closeResources();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
		}	
		
		return Liste;
	}

	@Override
	public void addUser(UserBean newUser) {
		try {			
			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt= yardimci_nesne.baglanti().prepareStatement("INSERT INTO registry_table(user_name,name,surname) VALUES(?,?,?)");
			yardimci_nesne.stmt.setString(1, newUser.getUserName());
			yardimci_nesne.stmt.setString(2, newUser.getName());
			yardimci_nesne.stmt.setString(3, newUser.getSurname());
			yardimci_nesne.stmt .executeUpdate();

			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("INSERT INTO users(user_name,user_pass) VALUES(?,?)");
			yardimci_nesne.stmt.setString(1, newUser.getUserName());
			yardimci_nesne.stmt.setString(2, newUser.getPassword());
			yardimci_nesne.stmt .executeUpdate();

			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("INSERT INTO user_roles(user_name,role_name) VALUES(?,?)");
			yardimci_nesne.stmt.setString(1, newUser.getUserName());
			yardimci_nesne.stmt.setString(2, newUser.getRoleName());
			yardimci_nesne.stmt .executeUpdate();
				
			yardimci_nesne.baglanti().commit();	
			yardimci_nesne.closeResources();
			
		    log.debug("Debug Message!");		    
		} catch (Exception e) {
			try {				
				yardimci_nesne.baglanti().rollback();				
				yardimci_nesne.closeResources();
				
			    log.error("Error Message!");
			} catch (SQLException hata) {
			    log.error("Error Message!");
			}
		}

	}

	@Override
	public void updateUser(UserBean newUser) {		
		try {
			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("UPDATE registry_table SET name=?,surname=? WHERE user_name=?");
			yardimci_nesne.stmt.setString(1, newUser.getName());
			yardimci_nesne.stmt.setString(2, newUser.getSurname());
			yardimci_nesne.stmt.setString(3, newUser.getUserName());
			yardimci_nesne.stmt .executeUpdate();
			
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("UPDATE users SET user_pass=? WHERE user_name=?");
			yardimci_nesne.stmt.setString(1, newUser.getPassword());
			yardimci_nesne.stmt.setString(2, newUser.getUserName());
			yardimci_nesne.stmt .executeUpdate();
			
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("UPDATE user_roles SET role_name=? WHERE user_name=?");
			yardimci_nesne.stmt.setString(1, newUser.getRoleName());
			yardimci_nesne.stmt.setString(2, newUser.getUserName());
			yardimci_nesne.stmt .executeUpdate();

			yardimci_nesne.baglanti().commit();	
			yardimci_nesne.closeResources();

			log.debug("Debug Message!");
		    						
		} catch (Exception e) {
			try {				
				yardimci_nesne.baglanti().rollback();				
				yardimci_nesne.closeResources();				
			    log.error("Error Message!");
			} catch (SQLException hata) {
			    log.fatal("Fatal Message!");

			}
		}

	}
	
	@Override
	public void deleteUser(String userName) {
		try {
			yardimci_nesne.baglanti().setAutoCommit(false);	
			yardimci_nesne.stmt=yardimci_nesne.baglanti().prepareStatement("delete from registry_table where user_name=?");
//			yardimci_nesne.stmt.setLong(1,newUser.);
			yardimci_nesne.stmt.addBatch();			
			yardimci_nesne.stmt.executeBatch();
			yardimci_nesne.baglanti().commit();	
			yardimci_nesne.closeResources();
//			yardimci_nesne.stmt .executeUpdate();	
			
		    log.debug("Debug Message!");		   
		} catch (Exception e) {
			log.error("Error Message!");
			
		}

	}

}
