package com.connection.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.property.PropertyConstant;

public class ConnectionHelper {	
	public PreparedStatement stmt;
	public ResultSet rs;
	Connection db_baglanti = null;	
	public Connection baglanti() throws SQLException{		
		
		if(PropertyConstant.dbDataSourceEnabled){
			try {
				InitialContext initContext = new InitialContext();
				DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/postgres");
				db_baglanti = ds.getConnection();
//				db_baglanti.setAutoCommit(false);					
				
			} catch (NamingException e1) {
				e1.printStackTrace();
			}
		}
		else{
			try {			
				Class.forName("org.postgresql.Driver");
				try {				
					//db_baglanti=DriverManager.getConnection("jdbc:postgresql://localhost:5432/ogrenciler","postgres","postgres");
					db_baglanti=DriverManager.getConnection(PropertyConstant.dbms+"://"+PropertyConstant.server+":"+PropertyConstant.portNumber+"/"+PropertyConstant.database,PropertyConstant.username,PropertyConstant.password);
//					db_baglanti.setAutoCommit(false);
				} catch (SQLException e) {					
					e.printStackTrace();
				}
				
			} catch (ClassNotFoundException e) {				
				e.printStackTrace();
			}	
		}		
		
		return db_baglanti;
	}
	
	public  PreparedStatement pstmt() throws SQLException{
		stmt= (PreparedStatement) db_baglanti.createStatement();		
		return stmt;		
	}
	
	public ResultSet rs() throws SQLException{
		rs=stmt.executeQuery();		
		return rs;		
	}
	
	public final void closeResources() throws SQLException{
		/*rs.close();
		stmt.close();*/
		db_baglanti.close();
		
	}
	
}
