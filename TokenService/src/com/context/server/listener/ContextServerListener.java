package com.context.server.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.property.PropertiesFile;

 
public class ContextServerListener implements ServletContextListener{
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Server start edildi...  ");	
		PropertiesFile propFile=new PropertiesFile();
		propFile.connectionProperties();
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Server destroy edildi...  ");
	}
 	
}
